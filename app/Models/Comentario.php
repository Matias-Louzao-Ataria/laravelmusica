<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    use HasFactory;

    protected $fillable = ['content'];

    public function __construct()
    {
        
    }

    public function musica(){
        return $this->belongsTo(Musica::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
