<?php

namespace App\Http\Controllers;

use App\Models\Comentario;
use App\Models\Musica;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MusicaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('musica.create');
    }

    public function edit($id)
    {
        $musica = Musica::findOrFail($id);
        return view('musica.edit',['musica'=>$musica]);
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'title'=>'required|max:20',
            'info'=>'required',
            'url'=>'url|nullable'
        ]);
        $res = \DB::table('musicas')->where('id','=',$request->id)->update($data);
        return view('musica.edit')->with(['res'=>$res,'musica'=>Musica::findOrFail($request->id)]);
    }

    public function index()
    {
        return view('musica.all',['musica'=> Musica::all()]);
    }

    public function showAll()
    {
        return view('musica.showAll',['musica'=>Musica::all()]);
    }

    public function showUser()
    {
        return view('musica.showUser',['musica'=>auth()->user()->musicas()->get()]);
    }

    public function show($id)
    {
        if(isset($id)){
            $musica = Musica::findOrFail($id);
            return view('musica.single')->with(['music'=>$musica,'comentarios'=>Comentario::all()]);
        }else{
           return back();
        }
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'title'=>'required|max:20',
            'info'=>'required',
            'url'=>'url|nullable'
        ]);

        $musica = new Musica();

        $musica->fill($data);
        $musica->user_id = auth()->user()->id;
        $musica->save();
        return view('musica.create')->with(['success' => true]);
    }

    public function destroy($id)
    {
        if(request()->isMethod('DELETE')){
            try{
                $musica = Musica::findOrFail($id);
                $musica->comentarios()->delete();
                $musica->delete();
                return back();
            }catch(Exception $e){
                dd($e);
            }
        }
    }
}
