<?php

namespace App\Http\Controllers;

use App\Models\Comentario;
use Illuminate\Http\Request;
use App\Models\Musica;
use Exception;

class ComentarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');    
    }

    public function store(Request $request)
    {
        $musica_id = $request->musica_id;
        $musica = Musica::findOrFail($musica_id);

        $data = $request->validate([
            'content'=>'required'
        ]);

        $comentario = new Comentario();

        $comentario->fill($data);
        $comentario->user_id = auth()->user()->id;
        $comentario->musica_id = $musica_id;
        $comentario->save();
        return back()->with(['music'=>$musica,'comentarios'=> Comentario::all()]);
    }

    public function all()
    {
        return view('comentarios.all')->with(['comentarios'=>Comentario::all()]);
    }

    public function destroy($id)
    {
        if(request()->isMethod("DELETE")){
            try{
                $comentario = Comentario::findOrFail($id);
                $comentario->delete();
                return back();
            }catch(Exception $e){
                dd($e);
            }
        }
    }

    public function edit($id)
    {
        $comentario = Comentario::findOrFail($id);
        return view('comentarios.edit')->with(['comentario'=>$comentario]);
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'content'=>'required'
        ]);
        $res = \DB::table('comentarios')->where('id','=',$request->id)->update($data);
        return view('comentarios.edit')->with(['res'=>$res,'comentario'=>Comentario::findOrFail($request->id)]);
    }

    public function user()
    {
        return view('comentarios.userComments')->with(['comentarios'=>auth()->user()->comentarios()->get()]);
    }
}
