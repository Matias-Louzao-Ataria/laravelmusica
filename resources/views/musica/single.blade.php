@extends('layouts.app')

@section('content')
<table class="container">
    <tr>
        <td>
            <h2 class="font-weight-bold">Nombre:</h2> {{$music->title}}
        </td>
        @if(!Auth::guest() && ($music->user_id == auth()->user()->id || auth()->user()->role === 0))
            <td>
                <a href="/musica/edit/{{$music->id}}" class="btn btn-primary btn-block">Editar</a>
            </td>
        @endif
    </tr>
    <tr>
        <td class="text.wrap">
            <h2 class="font-weight-bold">Información:</h2> {{$music->info}}
        </td>
        @if(!Auth::guest() && ($music->user_id == auth()->user()->id || auth()->user()->role === 0))
            <td align="right">
                <form method="POST" action="{{route('musica.destroy',['id'=>$music->id])}}">
                    @csrf
                    @method('DELETE')
                    <input class="btn btn-block btn-danger" type="submit" value="Borrar">
                </form>
            </td>
        @endif
    </tr>
    @if(isset($music->url) && strlen($music->url))
    <tr>
        <td colspan="2">
        <h2 class="font-weight-bold">URL:</h2><a href="{{$music->url}}">{{$music->url}}</a>
        </td>
    </tr>
    @endif
    <tr>
        <td colspan="2">
            <form action="{{route('comentario.store')}}" method="POST">
                @csrf
                <label class="mt-5" for="content">Comenta algo!</label>
                <input class="form-control" type="text" name="content">
                @if($errors->has('content'))
                    <div class="text-danger">{{$errors->first('content')}}</div>
                @endif
                <input name="musica_id" type="hidden" value="{{$music->id}}">
                <input class="mt-3 btn btn-block btn-primary" type="submit" value="Comentar">
            </form>
        </td>
    </tr>
    @if(isset($comentarios) && count($comentarios) > 0)
        <tr>
            <td colspan="2">
                <div class="mt-2">
                    Comentarios
                </div>
            </td>
        </tr>
        @foreach($comentarios as $comentario)
            @if($comentario->musica_id === $music->id)
                <tr>
                    <td class="content">
                        <div class="row">
                            <div class="col">
                                {{App\Models\User::findOrFail($comentario->user_id)->name}}:
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <h1 class="text.wrap">{{$comentario->content}}</h1>
                            </div>
                        </div>
                    </td>
                    @if(!Auth::guest() && (auth()->user()->role == 0 || auth()->user()->id == $comentario->user_id))
                            <td align="right">
                                <form method="POST" action="{{route('comentarios.destroy',['id'=>$comentario->id])}}">
                                    @csrf
                                    @method('DELETE')
                                    <input class="btn btn-danger mb-2" type="submit" value="Borrar">
                                </form>
                            </td>
                    @endif
                </tr>
            @endif
        @endforeach
    @endif
</table>
@endsection