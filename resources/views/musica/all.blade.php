@extends('layouts.app')

@section('content')
    @if(count($musica) > 0)
        <div class="container">
            <table class="table table-borderless table-hover">
                <thead>
                    <tr>
                        <th colspan="2">
                            <a href="/musica/create" class="btn btn-block btn-success mb-3">Añade una canción!</a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($musica as $music)
                        <tr>
                            <td>
                                Nombre: {{$music->title}}
                            </td>
                            <td>
                                <a class="btn btn-primary btn-block" href="{{route('musica.show',['id'=>$music->id])}}">Mostrar</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text.wrap">
                                Información: {{$music->info}}
                            </td>
                        </tr>
                        @if(isset($music->url) && strlen($music->url))
                            <tr>
                                <td colspan="2">
                                    <a href="{{$music->url}}">{{$music->url}}</a>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="container">
            <div>
                <a href="{{route('musica.create')}}" class="btn btn-block btn-success mb-3">Añade una canción!</a>
            </div>
            <div class="text-center">
                No hay canciones que mostrar!
            </div>
        </div>
    @endif
@endsection