@extends('layouts.app')

@section('content')
    <div class="container">
        <div>
            <h1>
                Añade una nueva canción!
            </h1>
        </div>
        <form action="{{route('musica.store')}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nombre</label>
                <input type="text" name="title" class="form-control" value="{{old('title')}}">
                @if($errors->has('title'))
                    <div class="text-danger">{{$errors->first('title')}}</div>
                @endif
            </div>

            <div class="form-group">
                <label for="info">Información extra</label>
                <input type="text" name="info" class="form-control" value="{{old('info')}}">
                @if($errors->has('info'))
                    <div class="text-danger">{{$errors->first('info')}}</div>
                @endif
            </div>
            
            <div class="form-group">
                <label for="url">URL</label>
                <input type="text" name="url" class="form-control" value="{{old('url')}}">
                @if($errors->has('url'))
                    <div class="text-danger">{{$errors->first('url')}}</div>
                @endif
            </div>
            <input type="submit" class="btn btn-block btn-success" value="Añadir">
        </form>
        @if(isset($success))
            <div class="alert alert-primary mt-3" role="alert">
                Guardado con éxito!
            </div>
        @endif
    </div>
@endsection