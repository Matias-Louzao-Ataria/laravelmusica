@extends('layouts.app')

@section('content')
    @if(count($musica) > 0)
        <div class="container">
            <table class="table table-borderless table-hover">
                <tbody>
                    @foreach($musica as $music)
                        <tr>
                            <td>
                                Nombre: {{$music->title}}
                            </td>
                            <td>
                                <a href="/musica/edit/{{$music->id}}" class="btn btn-primary btn-block">Editar</a>
                            </td>
                        </tr>
                        </tr>
                        <tr>
                            <td class="text.wrap">
                                Información: {{$music->info}}
                            </td>
                            <td align="right">
                                <form method="POST" action="{{route('musica.destroy',['id'=>$music->id])}}">
                                    @csrf
                                    @method('DELETE')
                                    <input class="btn btn-block btn-danger" type="submit" value="Borrar">
                                </form>
                            </td>
                        @if(isset($music->url) && strlen($music->url))
                            <tr>
                                <td colspan="2">
                                    <a href="{{$music->url}}">{{$music->url}}</a>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="container">
            <div class="text-center">
            {{auth()->user()->name}}, todavía no has creado ninguna canción, hazlo ahora!
            </div>
            <div>
                <a href="{{route('musica.create')}}" class="btn btn-block btn-success mb-3">Añade una canción!</a>
            </div>
        </div>
    @endif
@endsection