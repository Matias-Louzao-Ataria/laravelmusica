@extends('layouts.app')

@section('content')
    @if(isset($musica) && $musica !== null)
        <div class="container">
            <div>
                <h1>
                    Añade una nueva canción!
                </h1>
            </div>
            <form action="{{route('muscia.update')}}" method="POST">
                @csrf
                @method('PUT')
                <input name="id" type="hidden" value="{{$musica->id}}">
                <div class="form-group">
                    <label for="title">Nombre</label>
                    <input type="text" name="title" class="form-control" value="{{$musica->title}}">
                    @if($errors->has('title'))
                        <div class="text-danger">{{$errors->first('title')}}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="info">Información extra</label>
                    <input type="text" name="info" class="form-control" value="{{$musica->info}}">
                    @if($errors->has('info'))
                        <div class="text-danger">{{$errors->first('info')}}</div>
                    @endif
                </div>

                    <div class="form-group">
                        <label for="url">URL</label>
                        @if(isset($musica->url))
                            <input type="text" name="url" class="form-control" value="{{$musica->url}}">
                        @else
                            <input type="text" name="url" class="form-control">
                        @endif
                        @if($errors->has('url'))
                            <div class="text-danger">{{$errors->first('url')}}</div>
                        @endif
                    </div>
                <input type="submit" class="btn btn-block btn-success" value="Confirmar">
            </form>
            @if(isset($res) && $res == 1)
                <div class="alert alert-primary mt-3" role="alert">
                    Editado con éxito!
                </div>
            @elseif(isset($res) && $res == 0)
                <div class="alert alert-primary mt-3" role="alert">
                    Nada ha cambiado!
                </div>
            @elseif(isset($res) && $res < 0 && $res > 1)
                <div class="alert alert-danger mt-3" role="alert">
                    Error
                </div>
            @endif
        </div>
    @endif
@endsection