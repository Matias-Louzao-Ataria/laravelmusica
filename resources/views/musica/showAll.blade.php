@extends('layouts.app')

@section('content')
    @if(count($musica) > 0)
        <div class="container">
            <table class="table table-borderless table-hover">
                <tbody>
                    @foreach($musica as $music)
                        @if(auth()->user()->role === 0)
                            <tr>
                                <td>
                                    De: {{App\Models\User::findOrFail($music->user_id)->name}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Nombre: {{$music->title}}
                                </td>
                                <td align="right">
                                    <form method="POST" action="{{route('musica.destroy',['id'=>$music->id])}}">
                                        @csrf
                                        @method('DELETE')
                                        <input class="btn btn-block btn-danger" type="submit" value="Borrar">
                                    </form>
                                </td>
                            </tr>
                            <tr>
                                <td class="text.wrap">
                                    Información: {{$music->info}}
                                </td>
                            </tr>
                            @if(isset($music->url) && strlen($music->url))
                                <tr>
                                    <td colspan="2">
                                        <a href="{{$music->url}}">{{$music->url}}</a>
                                    </td>
                                </tr>
                            @endif
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="container">
            <div class="text-center">
                No hay canciones que mostrar!
            </div>
            <div>
                <a href="{{route('musica.create')}}" class="btn btn-block btn-success mb-3">Añade una canción!</a>
            </div>
        </div>
    @endif
@endsection