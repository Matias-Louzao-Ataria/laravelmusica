@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Acciones</div>

                <div class="card-body">
                    <a href="/musica/show" class="btn btn-success">Tus canciones</a>
                    <a href="/comentarios/show" class="btn btn-primary">Tus comentarios</a>
                    @if(auth()->user()->role === 0)
                        <a href="/musica/show/all" class="btn btn-warning">Todos las canciones</a>
                        <a href="/comentarios/index" class="btn btn-warning">Todos los comentarios</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
