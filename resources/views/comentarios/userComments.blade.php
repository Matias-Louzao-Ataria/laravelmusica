@extends('layouts.app')

@section('content')
    @if(isset($comentarios) && count($comentarios) > 0)
        @foreach($comentarios as $comentario)
            <table class="container">
                <tr>
                    <td class="content">
                        <div class="row">
                            <div class="col">
                                en  {{App\Models\Musica::findOrFail($comentario->musica_id)->title}}:
                            </div>
                        </div>
                        <div class="row">
                            <div class="col mb-3">
                                <h1 class="text.wrap">{{$comentario->content}}</h1>
                            </div>
                        </div>
                    </td>
                    @if(auth()->user()->id == $comentario->user_id)
                        <td align="right" >
                            <a href="/comentarios/edit/{{$comentario->id}}" class="btn btn-primary d-inline">Editar</a>
                            <form method="POST" class="d-inline" action="{{route('comentarios.destroy',['id'=>$comentario->id])}}">
                                @csrf
                                @method('DELETE')
                                <input class="btn btn-danger" type="submit" value="Borrar">
                            </form>
                        </td>
                    @endif
                </tr>
            </table>
        @endforeach
    @elseif(isset($comentarios) && count($comentarios) <= 0)
        <table class="container">
            <tr>
                <td class="text-center">
                    {{auth()->user()->name}}, todavía no has comentado ninguna vez, hazlo ahora!
                </td>
            </tr>
            <tr>
                <td>
                    <a href="/" class="btn btn-block btn-success">Elige una canción para comentar</a>
                </td>
            </tr>
        </table>
    @endif

@endsection