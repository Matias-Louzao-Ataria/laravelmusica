@extends('layouts.app')

@section('content')
    @if(isset($comentario) && $comentario != null)
        <div class="container">
            <form action="{{route('comentarios.update')}}" method="POST">
                @csrf
                @method('PUT')
                <label class="mt-5" for="content">Contenido</label>
                <input class="form-control" type="text" name="content" value="{{$comentario->content}}">
                @if($errors->has('content'))
                    <div class="text-danger">{{$errors->first('content')}}</div>
                @endif
                <input name="id" type="hidden" value="{{$comentario->id}}">
                <input class="mt-3 btn btn-block btn-primary" type="submit" value="Editar">
            </form>
            @if(isset($res) && $res == 1)
                    <div class="alert alert-primary mt-3" role="alert">
                        Editado con éxito!
                    </div>
                @elseif(isset($res) && $res == 0)
                    <div class="alert alert-primary mt-3" role="alert">
                        Nada ha cambiado!
                    </div>
                @elseif(isset($res) && $res < 0 && $res > 1)
                    <div class="alert alert-danger mt-3" role="alert">
                        Error
                    </div>
                @endif
            @endif
        </div>
@endsection